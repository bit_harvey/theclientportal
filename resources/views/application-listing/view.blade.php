@extends('layouts.app-sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ URL::previous() }}">Application Listing</a></li>
                <li class="breadcrumb-item active" aria-current="page">Listing</li>
            </ol>
        
                <div class="card">
                    <div class="card-header bg-white">
                        <h5 class="card-title">Application Listing<a href="{{ route('client-manager.process', $view_id) }}" type="button" class="btn btn-dark float-right">Process</a><a href="{{ route('application-listing.view', $view_id) }}" type="button" class="btn btn-dark float-right mr-2">Listing</a></h5>
                    </div>
                    
                        <div class="card-body">
                             @livewire('application-listing.view', ['view_id' => $view_id])
                        </div>
                        
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection