@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div class="row">
            <div class="col-sm-4 mb-4">
                <div class="card">
                <div class="card-body">
                    <div>
                        <h5 class="card-title">Online Application</h5><hr>
                        <div class="row">
                            <div class="col-md-4 align-middle text-center p-0" style="font-size: 60px;">
                                <i class="fa fa-file"></i>
                            </div>
                        <div class="col">
                            <p class="card-text">Apply Now on our Online Registration to be an Elite Member editable <a href="https://www.thailandelitevisas.com/online-application/#download-forms">Application forms</a> (PDF) are available to download below.</p>
                        </div>
                        </div> 
                        <a href="{{ route('online-registration') }}" class="btn btn-primary">Apply Now</a>
                    </div>
                    
                </div>
                </div>
            </div>
            <div class="col-sm-4 mb-4">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Client Portal</h5><hr>
                    <div class="row">
                        <div class="col-md-4 align-middle text-center p-0" style="font-size: 60px;">
                            <i class="fa fa-sign-in"></i>
                        </div>
                        <div class="col">
                            <p class="card-text">Login to access Client portal where you can manage your info and your application status.</p>
                        </div>
                    </div>
                    <a href="{{ route('login') }}" class="btn btn-primary mt-4">Client Login</a>
                </div>
                </div>
            </div>
            <div class="col-sm-4 mb-4">
                <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Sub-Agent Portal</h5><hr>
                    <div class="row">
                        <div class="col-md-4 align-middle text-center p-0" style="font-size: 60px;">
                            <i class="fa fa-user"></i>
                        </div>
                    <div class="col">
                         <p class="card-text">Login to access Sub-Agent portal where you can manage the list of reffered clients and check their application status.</p>
                    </div>
                    </div>
                    <a href="{{ route('login') }}" class="btn btn-primary mt-4">Sub-Agent Login</a>
                </div>
                </div>
            </div>
            
            </div>
            <div class="footer text-center mt-4">Copyright 	&#169; 2021 <a href="https://www.thailandelitevisas.com/" target="_blank">Thailand Elite Visas</a>. All Rights Reserved</div>
        </div>
    </div>
</div>
@endsection
