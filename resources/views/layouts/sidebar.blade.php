<div class="bg-light border-right" id="sidebar-wrapper">
  <div class="sidebar-heading"> 
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('logo.png') }}" >
        </a></div>
  <div class="list-group list-group-flush">
    <a href="{{ route('home') }}" class="list-group-item list-group-item-action {{ Request::url() == url('/home') ? 'active' : 'bg-light' }}"><i class="fa fa-home"></i> Dashboard</a>
    @if( auth::user()->role == 'Client')
    <a href="{{ route('client-manager.edit', Auth::user()->client_id) }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'client-manager' ? 'active' : 'bg-light' }}"><i class="fa fa-user"></i> Profile</a>
    <a href="#" class="list-group-item list-group-item-action bg-light"><i class="fa fa-book"></i> Documents</a>
    @elseif( auth::user()->role == 'Admin') 
    <a href="{{ route('application-registration') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'application-registration' ? 'active' : 'bg-light' }}"><i class="fa fa-file"></i> Application Registration</a>
    <a href="{{ route('client-manager') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'client-manager' ? 'active' : 'bg-light' }}"><i class="fa fa-user"></i> Clients</a>
    <a href="{{ route('application-listing') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'application-listing' ? 'active' : 'bg-light' }}"><i class="fa fa-list"></i> Application Listing</a>
    <a href="{{ route('user-manager') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'user-manager' ? 'active' : 'bg-light' }}"><i class="fa fa-user"></i> User Manager</a>
    <a href="{{ route('settings') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'settings' ? 'active' : 'bg-light' }}"><i class="fa fa-cog"></i> Settings</a>
    <a href="{{ route('announcement') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'announcement' ? 'active' : 'bg-light' }}"><i class="fa fa-user"></i> Announcement</a>
    <a href="{{ route('audit-trail') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'audit-trail' ? 'active' : 'bg-light' }}"><i class="fa fa-list"></i> Audit Trail</a>
    @else 
    <a href="{{ route('application-registration') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'application-registration' ? 'active' : 'bg-light' }}"><i class="fa fa-file"></i> Application Registration</a>
    <a href="{{ route('client-manager') }}" class="list-group-item list-group-item-action {{ Request::segment(1) === 'client-manager' ? 'active' : 'bg-light' }}"><i class="fa fa-user"></i> Clients</a>
    @endif
  </div>
</div>
  