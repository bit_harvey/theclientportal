<!DOCTYPE html>
<html>
<head>
    <title>Client Portal - Thailand Elite Visas</title>
</head>
<body>
    <h1>Client Portal - Thailand Elite Visas</h1>
    <p>Please use below credential below to access your profile in the Client Portal.</p>
   
    <p>Username: {{ $details['email'] }}</p>
    <p>Password: {{ $details['password'] }}</p>

    <p>Thank You.</p>
</body>
</html>