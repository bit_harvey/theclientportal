@extends('layouts.app-sidebar')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">User Manager</li>
            </ol>
        </nav>
                <div class="card">
                    <div class="card-header bg-white">
                        <h5 class="card-title">User Manager
                        <a href="{{route('user-manager.add')}}" type="button" class="btn btn-dark float-right">+ Add User</a>
                        </h5>
                    </div>
                        @livewire('user-manager.index')
                </div>
    
        </div>
    </div>
</div>
@endsection