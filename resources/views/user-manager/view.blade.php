@extends('layouts.app-sidebar')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('user-manager')}}">User Manager</a></li>
                <li class="breadcrumb-item active" aria-current="page">View User</li>
            </ol>
        </nav>

                <div class="card">
                    <div class="card-header bg-white">
                        <h5 class="card-title">View User</h5>
                    </div>
                    <div class="card-body">
                    @livewire('user-manager.view', ['view_id' => $view_id])
                    </div>    
                        
                </div>
    
        </div>
    </div>
</div>
@endsection