@extends('layouts.app-sidebar')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Settings</li>
                </ol>
                <div class="card">
                    <div class="card-header bg-white">
                        <h5 class="card-title">Settings</h5>
                    </div>
                        <livewire:settings.index />

                </div>
        
 
        </div>
    </div>
</div>
@endsection