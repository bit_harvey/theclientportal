@extends('layouts.app-sidebar')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Application Registration</li>
            </ol>
        </nav>
                <div class="card">
                    <div class="card-header bg-white">
                        <h5 class="card-title">Application Registration<a href="{{ route('application-registration.add') }}" type="button" class="btn btn-dark float-right">+ Add Application</a></h5>
                        
                    </div>
                        <livewire:application-registration.index />

                </div>
        
 
        </div>
    </div>
</div>
@endsection