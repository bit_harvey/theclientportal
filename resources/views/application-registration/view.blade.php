@extends('layouts.app-sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ URL::previous() }}">Application Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page">View Application</li>
            </ol>

                <div class="card">
                    <div class="card-header bg-white">
                        <h5>View Application<a href="" type="button" class="btn btn-dark float-right">Attached Files</a></h5>
                    </div>
                        <div class="card-body">
                        @livewire('application-registration.view', ['view_id' => $view_id])
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection