@extends('layouts.app-sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route('application-registration')}}">Application Registration</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Application</li>
            </ol>
                <div class="card">
                    <div class="card-body">
            
                        <h5 class="card-title">Add Application</h5><hr>

                        <livewire:application-registration.add />

                </div>
        
 
        </div>
    </div>
</div>
@endsection