@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">

                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Apply to be an Elite Member</h5>
                        <small class="text-muted"><span class="text-danger">*</span> Indicates required fields. Editable <a href="https://www.thailandelitevisas.com/online-application/#download-forms">Application forms</a> (PDF) are available to download below.Watch our guide on <a href="https://player.vimeo.com/video/467165925?background=1">How to Apply</a>. </small>
                        <hr>
                        <livewire:online-registration />
                    </div>
                </div>
        
 
        </div>
    </div>
</div>
@endsection
