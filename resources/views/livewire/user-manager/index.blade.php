<div class="table-responsive">

<div class="p-2">
    @if (session()->has('message'))
            <div class="alert alert-success">
            {{ session('message') }}
            </div>
    @endif
    @if ($errors->any())
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
        {{ $error }}
        </div>
        @endforeach
    @endif
    <div class="form-row">
        <div class="form-group col-md-3">
            <label>Search </label>
            <input wire:model="search"  class="form-control" type="text" placeholder="Search" />
        </div>
        <div class="form-group col-md-3">
        <label><i class="fa fa-search"></i> Role </label>
        <select wire:model="role" class="form-control">
            <option value="Admin">Admin</option>
            <option value="Agent">Agent</option>
            <option value="Client">Client</option>
        </select>
        </div>
    </div>
</div>
    <table class="card-table table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Active</th>
            <th scope="col">Created at</th>
            <th scope="col">Updated at</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->name}}</td>
            <td>{{ $user->email}}</td>
            <td>{{ $user->active}}</td>
            <td>
            @if($user->created_at == "")
            @else 
            {{ date('F j, Y, g:i', strtotime($user->created_at)) }}
            @endif  
            </td>
            <td> 
            @if($user->updated_at == "")
            @else
                {{ date('F j, Y, g:i', strtotime($user->updated_at)) }}
            @endif
            </td>
            <td>

            <div class="btn-group">
                <a href="{{ route('user-manager.view', $user->id) }}" type="button" class="btn btn-primary">
                    View
                </a>
                
                <div class="dropdown-menu">
                   
                </div>
            </div>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
   <hr>
   <div class="pagination ml-2">
        {{ $users->links('livewire.pagination.livewire-pagination')}}
    </div>
   