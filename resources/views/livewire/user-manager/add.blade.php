<div>
    <form wire:submit.prevent="add()">
        @if (session()->has('message'))
                 <div class="col">
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                </div>
            @endif
         <div class="form-group">
            <label>Name</label>
            <input wire:model="name" type="text" value="" class="form-control @error('name') is-invalid @enderror">
            @error('name') <span class="invalid-feedback" role="alert">{{ $message }}</span> @enderror
        </div>

        <div class="form-group">
            <label>Email address</label>
            <input wire:model="email" type="email" value="" class="form-control @error('email') is-invalid @enderror" >
            @error('email') <span class="invalid-feedback" role="alert">{{ $message }}</span> @enderror

        </div>

        <div class="form-group">
            <label>Password</label>
            <input wire:model="password" type="text" value="" class="form-control @error('password') is-invalid @enderror">
            @error('password') <span class="invalid-feedback" role="alert">{{ $message }}</span> @enderror

        </div>

      
        <div class="form-group">
            <label>Role</label>
            <select wire:model="role" class="form-control @error('role') is-invalid @enderror">
                <option value="">Select Role</option>
                <option value="Admin">Admin</option>
                <option value="Agent">Agent</option>
            </select>
            @error('role') <span class="invalid-feedback" role="alert">{{ $message }}</span> @enderror

        </div>

        <div class="form-group">
            <label>Status</label>
            <select wire:model="active" class="form-control @error('active') is-invalid @enderror">
                <option value="">Select Status</option>
                <option value="0">Active</option>
                <option value="1">Inactive</option>
            </select>
            @error('active') <span class="invalid-feedback" role="alert">{{ $message }}</span> @enderror

        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
          
            </div>
        </div>


    </form>
</div>
