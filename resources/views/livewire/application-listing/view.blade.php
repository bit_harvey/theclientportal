<div>
    <form wire:submit.prevent="update()">
    @foreach($application_listings as $application_listing)
         <div class="form-group">
            <div class="row">
                <div class="col">
                    <label>First Name</label>
                    <input type="text" value="{{ $application_listing->fname }}" class="form-control" readonly>
                </div>
                <div class="col">
                    <label>Last Name</label>
                    <input type="text" value="{{ $application_listing->lname }}" class="form-control" readonly>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label>Email address</label>
            <input type="email" value="{{ $application_listing->email }}" class="form-control" readonly>
        </div>
    @endforeach
       
        <div class="form-group">
            <label>Date Sent</label>
            <input type="date" wire:model="date_sent" class="form-control  @error('date_sent') is-invalid @enderror">
        </div>

        <div class="form-group">
            <label>Date Received</label>
            <input type="date" wire:model="date_received" class="form-control  @error('date_received') is-invalid @enderror">
        </div>

        <div class="form-group">
            <label>Date Applied</label>
            <input type="date" wire:model="date_applied" class="form-control  @error('date_applied') is-invalid @enderror">
        </div>


        <div class="form-group">
            <label>Payment Status</label>
            <select wire:model="payment_status" class="form-control">
                <option value="0">Unpaid</option>
                <option value="1">Paid</option>
            </select>
        </div>

        <div class="form-group">
            <label>Status</label>
            <select wire:model="application_status" class="form-control">
                <option value="0">On Progress</option>
                <option value="1">Approved</option>
                <option value="2">Disapproved</option>
                <option value="3">Cancelled</option>
                <option value="4">Expired</option>

            </select>
        </div>

        <div class="form-group">
            <div class="row">
                <div class="col-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            @if (session()->has('message'))
                 <div class="col">
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                </div>
            @endif
            </div>
        </div>


    </form>
</div>
