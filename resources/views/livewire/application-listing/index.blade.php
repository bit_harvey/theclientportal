<div class="table-responsive">
<div class="p-2">
@if (session()->has('message'))
        <div class="alert alert-success">
        {{ session('message') }}
        </div>
@endif
@if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
      {{ $error }}
    </div>
    @endforeach
@endif
    <div class="form-row">
        <div class="form-group col-md-3 ">
            <label><i class="fa fa-search"></i> Search </label>
            <input wire:model="search"  class="form-control" type="text" placeholder="Search" />
            </div>
        <div class="form-group col-md-3">
            <label>Fields </label>
            <select wire:model="fields" class="form-control">
                <option value="application_listings.date_sent">Date Sent</option>
                <option value="application_listings.date_received">Date Received</option>
                <option value="application_listings.date_applied">Date Applied</option>
            </select>
            </div>
            <div class="col-md-3">
            <label>from: </label>
                <input wire:model="sentfrom" type="date" class="form-control">
            </div>
            <div class="col-md-3">
            <label>to: </label>
                <input  wire:model="sentto" type="date" class="form-control">
            </div>
        </div>
</div>

    <table class="card-table table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Date Sent</th>
            <th scope="col">Date Received</th>
            <th scope="col">Date Applied</th>
            <th scope="col">Payment</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($application_listings as $application_listing)
        <tr>
            <td>{{ $application_listing->fname }} {{ $application_listing->lname }}</td>
            <td>{{ $application_listing->email }}</td>
            <td>{{ date('F j, Y', strtotime($application_listing->date_sent)) }}</td>
            <td>{{ date('F j, Y', strtotime($application_listing->date_received)) }}</td>
            <td>
            @if ($application_listing->date_applied == "")
                
            @else
                {{ date('F j, Y', strtotime($application_listing->date_applied)) }}
            @endif
            </td>
            <td>
                 @if ($application_listing->payment_status  == 0)
                <span>Unpaid</span>
                @else ($application_listing->payment_status  == 1)
                    <span>Paid</span>
                @endif
                <br>
            </td>
            <td>
                @if ($application_listing->application_status  == 0)
                <span class="badge badge-warning">On Progress</span>
                @elseif ($application_listing->application_status  == 1)
                        <span class="badge badge-info">Approve</span>
                @elseif ($application_listing->application_status  == 2)
                        <span class="badge badge-danger">Disapproved</span>
                @elseif ($application_listing->application_status  == 3)
                        <span class="badge badge-danger">Cancelled</span>
                @else ($application_listing->application_status  == 4)
                    <span class="badge badge-dark">Expired</span>
                @endif
            
            </td>
            <td>
            
                <div>   
                    <a  href="{{ route('application-listing.view', $application_listing->application_id) }}" class="btn btn-primary">View</a>
            
                </div>
                
                </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    <hr>
    <div class="pagination ml-2">
    {{ $application_listings->links('livewire.pagination.livewire-pagination')}}
    </div>