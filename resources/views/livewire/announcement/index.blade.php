<div class="table-responsive">
    <table class="card-table table">
        <tr>
            <th>Title</th>
            <th>Announcement</th>
            <th>Action</th>
        </tr>
        <tbody>
         @foreach($announcements as $announcement)
            <tr>
                <td>{{ $announcement->title }}</td>
                <td>{{ $announcement->description }}</td>
                <td><a href="{{ route('announcement.view', $announcement->id) }}" type="button" class="btn btn-primary">View</a></td>
            </tr>
         @endforeach
        </tbody>
    </table>
    <hr>
    <div class="pagination ml-2">
    {{ $announcements->links('livewire.pagination.livewire-pagination')}}
    </div>
</div>
