<div>
    <form wire:submit.prevent="add()">
        @if (session()->has('message'))
                 <div class="col">
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                </div>
            @endif

         <div class="form-group">
            <label>Title</label>
            <input wire:model="title" type="text" value="" class="form-control @error('title') is-invalid @enderror">
            @error('title') <span class="invalid-feedback" role="alert">{{ $message }}</span> @enderror
        </div>

        <div class="form-group" wire:ignore>
            <label>Description</label>
            <input id="body" type="hidden" name="content">
            <trix-editor wire:model="description" input="body"></trix-editor>
            @error('description') <span class="invalid-feedback" role="alert">{{ $message }}</span> @enderror

        </div>
    

        <div class="form-group">
            <div class="row">
                <div class="col-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
          
            </div>
        </div>


    </form>
</div>
