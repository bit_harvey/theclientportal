<div class="table-responsive">
@include('livewire.application-registration.approve')
@include('livewire.application-registration.assign')
@include('livewire.application-registration.decline')

<div class="p-2">
@if (session()->has('message'))
        <div class="alert alert-success">
        {{ session('message') }}
        </div>
@endif
@if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
      {{ $error }}
    </div>
    @endforeach
@endif
    <div class="form-row">
        <div class="form-group col-md-3">
        <label><i class="fa fa-search"></i> Search </label>
        <input wire:model="search"  class="form-control" type="text" placeholder="Search" />
        </div>
        <div class="form-group col-md-3">
            <label>Fields </label>
            <select wire:model="fields" class="form-control">
                <option value="application_registrations.fname">Firstname</option>
                <option value="application_registrations.lname">Lastname</option>
                <option value="application_registrations.email">Email</option>
                <option value="users.name">Agent</option>
            </select>
            </div>
       
            <div class="col-md-3">
            <label>from: </label>
                <input wire:model="from" type="date" class="form-control">
            </div>
            <div class="col-md-3">
            <label>to: </label>
                <input  wire:model="to" type="date" class="form-control">
        </div>
        <div class="form-group col-md-3">
            <label>Status </label>
            <select wire:model="status" class="form-control">
                <option value="0">Pending</option>
                <option value="2">Declined</option>
            </select>
            </div>
        </div>
</div>
    <table class="card-table table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Agent</th>
            <th scope="col">Status</th>
            <th scope="col">Created_at</th>
            @if(Auth::user()->role == "Agent")
            @else
            <th scope="col">Action</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($application_registrations as $application_registration)
        <tr>
            <td>{{ $application_registration->fname }} {{ $application_registration->lname }}</td>
            <td>{{ $application_registration->email }}</td>
            <td>{{ $application_registration->name }}</td>
            <td>
            @if($application_registration->status == "0")
                <span class="badge badge-warning">Pending</span>
            @else
                <span class="badge badge-danger">Decline</span>
            @endif
            </td>
            @if(Auth::user()->role == "Agent")
            @else
            <td>{{ date('F j, Y, g:i', strtotime($application_registration->created_at)) }}</td></td>
            <td>
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action
                </button>
                
                <div class="dropdown-menu">
                    @if($application_registration->status != 2 )
                    <a class="dropdown-item"  href="{{ route('application-registration.view', $application_registration->id) }}">View</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#approvedmodal" wire:click="approved({{ $application_registration->id }})">Approve</a>
                    <a class="dropdown-item"  data-toggle="modal" data-target="#assignmodal" wire:click="assign({{ $application_registration->id }})">Assign</a>
                    <a class="dropdown-item"  wire:click="decline({{ $application_registration->id }})">Decline</a>
                    @else  
                    <a class="dropdown-item"  href="{{ route('application-registration.view', $application_registration->id) }}">View</a>
                    <a class="dropdown-item"  wire:click="restore({{ $application_registration->id }})">Restore</a>
                    @endif
                </div>
            </div>
                   
            </td>
            @endif
            
        </tr>
        @endforeach
        </tbody>
    </table>
    <hr>
    <div class="pagination ml-2">
    {{ $application_registrations->links('livewire.pagination.livewire-pagination')}}
    </div>
   
