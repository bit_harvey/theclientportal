<div>
<table class="table table-bordered">
  <tbody>
  @foreach($application_registrations as $application_registration)
    <tr>
      <th scope="row" style="width:40%">Name</th>
      <td>{{ $application_registration->fname }} {{ $application_registration->lname }}
      
        <small class="text-muted">
        @if ($application_registration->status  == 0)
          <span class="badge badge-warning">Pending</span>
        @elseif ($application_registration->status  == 1)
                <span class="badge badge-success">Approved</span>
        @else ($application_registration->status  == 2)
            <span class="badge badge-danger">Declined</span>
        @endif
        </small>
      </td>
    </tr>
    <tr>
      <th scope="row">Email address</th>
      <td>{{ $application_registration->email }}</td>
    </tr>
    <tr>
      <th scope="row">Nationality</th>
      <td>{{ $application_registration->nationality }}</td>
    </tr>
    <tr>
      <th scope="row">Phone Number</th>
      <td>{{ $application_registration->country_number }} {{ $application_registration->phone_number }}</td>
    </tr>
    <tr>
      <th scope="row">My Preferred Program</th>
      <td>{{ $application_registration->program }}</td>
    </tr>
    <tr>
      <th scope="row">My Location</th>
      <td>{{ $application_registration->location }}</td>
    </tr>
    <tr>
      <th scope="row">If you are in Thailand, please select what province you are in</th>
      <td>{{ $application_registration->province }}</td>
    </tr>
    <tr>
      <th scope="row">Residential Address</th>
      <td>{{ $application_registration->residential_address }}</td>
    </tr>
    <tr>
      <th scope="row">Current Visa</th>
      <td>{{ $application_registration->current_visa }}</td>
    </tr>
    <tr>
      <th scope="row">If you have a visa, please provide place of issue and expiry date:</th>
      <td>{{ $application_registration->question1 }}</td>
    </tr>
    <tr>
      <th scope="row">Applicant have at least 3 empty pages in my passport (not include endorsement page)</th>
      <td>{{ $application_registration->question2 }}</td>
    </tr>
    <tr>
      <th scope="row">Where would you prefer to affix your Elite visa? (this is not a confirmation of your option)</th>
      <td>{{ $application_registration->question3 }}</td>
    </tr>
     <tr>
      <th scope="row">Do you own a property in Thailand?</th>
      <td>{{ $application_registration->question4 }}</td>
    </tr>
    <tr>
      <th scope="row">Have you ever overstay in Thailand for more than once?</th>
      <td>{{ $application_registration->question5 }}</td>
    </tr>

    <tr>
      <th scope="row">Who is applying?</th>
      <td>{{ $application_registration->question6 }}</td>
    </tr>

    <tr>
      <th scope="row">Name of contact person or introducer, if any.</th>
      <td>{{ $application_registration->question7 }}</td>
    </tr>
    <tr>
      <th scope="row">When do you plan to obtain your Elite Visa?</th>
      <td>{{ $application_registration->question8 }}</td>
    </tr>
    <tr>
      <th scope="row">Upload Application Form (Available below)</th>
      <td>
        @foreach (json_decode($application_registration->upload_application_form, true) as $upload_application_form)
        @if (pathinfo(storage_path('$upload_application_form'), PATHINFO_EXTENSION) == 'docx')
          <div class="mb-2"><i class="fa fa-file-doc-o" aria-hidden="true" style="font-size:50px;"></i></div><div><a href="{{ asset('storage/' . $upload_application_form) }}" target="_blank" class="btn btn-primary">View</a></div>
        @elseif (pathinfo(storage_path('$upload_application_form'), PATHINFO_EXTENSION) == 'pdf')
          <div class="mb-2"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:50px;"></i></div><div><a href="{{ asset('storage/' . $upload_application_form) }}" target="_blank" class="btn btn-primary">View</a></div>
        @else
          <a href="{{ asset('storage/' . $upload_application_form) }}" target="_blank"><img src="{{ asset('storage/' . $upload_application_form) }}" heigh="50" width="50"></a>
        @endif
        @endforeach

        
    </td>
    </tr>
    <tr>
      <th scope="row">Upload Passport</th>
      <td>
      @foreach (json_decode($application_registration->upload_passport, true) as $upload_passport)
        @if (pathinfo(storage_path('$upload_passport'), PATHINFO_EXTENSION) == 'docx')
        <div class="mb-2"><i class="fa fa-file-doc-o" aria-hidden="true" style="font-size:50px;"></i></div><div><a href="{{ asset('storage/' . $upload_passport) }}" target="_blank" class="btn btn-primary">View</a></div>
        @elseif (pathinfo(storage_path('$upload_passport'), PATHINFO_EXTENSION) == 'pdf')
          <div class="mb-2"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:50px;"></i></div><div><a href="{{ asset('storage/' . $upload_passport) }}" target="_blank" class="btn btn-primary">View</a></div>
        @else
          <a href="{{ asset('storage/' . $upload_passport) }}" target="_blank"><img src="{{ asset('storage/' . $upload_passport) }}" heigh="50" width="50"></a>
        @endif
      @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">File Sharing URL/Link</th>
      <td>
      {{ $application_registration->upload_share_link }}
      </td>
    </tr>
    <tr>
      <th scope="row">Upload Photo</th>
      <td>
      @foreach (json_decode($application_registration->upload_photo, true) as $upload_photo)
      <a href="{{ asset('storage/' . $upload_photo) }}" target="_blank"><img src="{{ asset('storage/' . $upload_photo) }}" heigh="50" width="50"></a>
                @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">Requirements to Enroll an Immediate Family Member</th>
      <td>
      @foreach (json_decode($application_registration->upload_photo, true) as $upload_photo)
        <a href="{{ asset('storage/' . $upload_photo) }}" target="_blank"><img src="{{ asset('storage/' . $upload_photo) }}" heigh="50" width="50">
                @endforeach
      </td>
    </tr>
    <tr>
      <tr>
      <th scope="row">Child Birth Certificate (Child Applicant Only)</th>
      <td>
      @foreach (json_decode($application_registration->upload_child_birth_certificate, true) as $upload_child_birth_certificate)
      <a href="{{ asset('storage/' . $upload_child_birth_certificate) }}" target="_blank"><img src="{{ asset('storage/' . $upload_child_birth_certificate) }}" heigh="50" width="50"></a>
                @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">Others</th>
      <td>
      @foreach (json_decode($application_registration->upload_others, true) as $upload_others)
      <a href="{{ asset('storage/' . $upload_others) }}" target="_blank"><img src="{{ asset('storage/' . $upload_others) }}" heigh="50" width="50"></a>
                @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">Agent</th>
      <td>
       {{ $application_registration->name }}
      </td>
    </tr>
    <tr>
      <th scope="row">Comment</th>
      <td>
      {{ $application_registration->comment }}
      </td>
    </tr>
      <th scope="row">I agree to receiving marketing and promotional materials.</th>
      <td>
         @if($application_registration->agreement == '0')
        
        @else
        {{ $application_registration->agreement }}
        @endif
      </td>
    </tr>
  </tbody>
</table>

 
    

        @endforeach
</div>



