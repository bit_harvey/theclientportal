<!-- Modal -->
<div wire:ignore.self class="modal fade" id="assignmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
       <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Assign</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
             <h5 class="mb-3">You can assign a sub-agent.</h5>

             <form class="form-group">
             <label for="">Assign to sub-agent:</label>
             <input type="hidden" class="form-control" wire:model="client_id" placeholder="client_id">
                <select wire:model="agent" class="form-control @error('location') is-invalid @enderror">
                    <option value="">Select Agent</option>
                    @foreach($sub_agents as $sub_agent)
                    <option value="{{ $sub_agent->id }}">{{ $sub_agent->name }}</option>
                    @endforeach
                   
                </select>
                @error('sub_agent') <span class="text-danger error">{{ $message }}</span>@enderror
                </form>
            </div>  

            
           
            <div class="modal-footer">
                <button type="button" wire:click.prevent="cancel()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" wire:click.prevent="assign_store()" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-user"></i> Assign</button>
            </div>
       </div>
    </div>
</div>