<div>
<table class="table table-bordered">
  <tbody>
  @foreach($clients as $client)
    <tr>
      <th scope="row" style="width:40%">Name</th>
      <td>{{ $client->fname }} {{ $client->lname }}
      
        <small class="text-muted">
        @if ($client->status  == NULL)
        <span class="badge badge-warning">Pending</span>
        @elseif ($client->status  == 1)
                <span class="badge badge-success">Approved</span>
        @else ($client->status  == 2)
            <span class="badge badge-danger">Declined</span>
        @endif
        </small>
      </td>
    </tr>
    <tr>
      <th scope="row">Email address</th>
      <td>{{ $client->email }}</td>
    </tr>
    <tr>
      <th scope="row">Nationality</th>
      <td>{{ $client->nationality }}</td>
    </tr>
    <tr>
      <th scope="row">Phone Number</th>
      <td>{{ $client->country_number }} {{ $client->phone_number }}</td>
    </tr>
    <tr>
      <th scope="row">My Preferred Program</th>
      <td>{{ $client->program }}</td>
    </tr>
    <tr>
      <th scope="row">My Location</th>
      <td>{{ $client->location }}</td>
    </tr>
    <tr>
      <th scope="row">If you are in Thailand, please select what province you are in</th>
      <td>{{ $client->province }}</td>
    </tr>
    <tr>
      <th scope="row">Residential Address</th>
      <td>{{ $client->residential_address }}</td>
    </tr>
    <tr>
      <th scope="row">Current Visa</th>
      <td>{{ $client->current_visa }}</td>
    </tr>
    <tr>
      <th scope="row">If you have a visa, please provide place of issue and expiry date:</th>
      <td>{{ $client->question1 }}</td>
    </tr>
    <tr>
      <th scope="row">Applicant have at least 3 empty pages in my passport (not include endorsement page)</th>
      <td>{{ $client->question2 }}</td>
    </tr>
    <tr>
      <th scope="row">Where would you prefer to affix your Elite visa? (this is not a confirmation of your option)</th>
      <td>{{ $client->question3 }}</td>
    </tr>
     <tr>
      <th scope="row">Do you own a property in Thailand?</th>
      <td>{{ $client->question4 }}</td>
    </tr>
    <tr>
      <th scope="row">Have you ever overstay in Thailand for more than once?</th>
      <td>{{ $client->question5 }}</td>
    </tr>

    <tr>
      <th scope="row">Who is applying?</th>
      <td>{{ $client->question6 }}</td>
    </tr>

    <tr>
      <th scope="row">Name of contact person or introducer, if any.</th>
      <td>{{ $client->question7 }}</td>
    </tr>
    <tr>
      <th scope="row">When do you plan to obtain your Elite Visa?</th>
      <td>{{ $client->question8 }}</td>
    </tr>
    <tr>
      <th scope="row">Upload Application Form (Available below)</th>
      <td>
        @foreach (json_decode($client->upload_application_form, true) as $upload_application_form)
          <div class="mb-2"><i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:50px;"></i></div><div><a href="{{ asset('storage/' . $upload_application_form) }}" target="_blank" class="btn btn-primary">View</a></div>
        @endforeach
    </td>
    </tr>
    <tr>
      <th scope="row">Upload Passport</th>
      <td>
      @foreach (json_decode($client->upload_passport, true) as $upload_passport)
        <a href="{{ asset('storage/' . $upload_passport) }}" target="_blank"><img src="{{ asset('storage/' . $upload_passport) }}" heigh="50" width="50"></a>
      @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">File Sharing URL/Link</th>
      <td>
      {{ $client->upload_share_link }}
      </td>
    </tr>
    <tr>
      <th scope="row">Upload Photo</th>
      <td>
      @foreach (json_decode($client->upload_photo, true) as $upload_photo)
      <a href="{{ asset('storage/' . $upload_photo) }}" target="_blank"><img src="{{ asset('storage/' . $upload_photo) }}" heigh="50" width="50"></a>
                @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">Requirements to Enroll an Immediate Family Member</th>
      <td>
      @foreach (json_decode($client->upload_photo, true) as $upload_photo)
        <a href="{{ asset('storage/' . $upload_photo) }}" target="_blank"><img src="{{ asset('storage/' . $upload_photo) }}" heigh="50" width="50">
                @endforeach
      </td>
    </tr>
    <tr>
      <tr>
      <th scope="row">Child Birth Certificate (Child Applicant Only)</th>
      <td>
      @foreach (json_decode($client->upload_child_birth_certificate, true) as $upload_child_birth_certificate)
      <a href="{{ asset('storage/' . $upload_child_birth_certificate) }}" target="_blank"><img src="{{ asset('storage/' . $upload_child_birth_certificate) }}" heigh="50" width="50"></a>
                @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">Others</th>
      <td>
      @foreach (json_decode($client->upload_others, true) as $upload_others)
      <a href="{{ asset('storage/' . $upload_others) }}" target="_blank"><img src="{{ asset('storage/' . $upload_others) }}" heigh="50" width="50"></a>
      @endforeach
      </td>
    </tr>
    <tr>
      <th scope="row">Agent</th>
      <td>
       {{ $client->name }}
      </td>
    </tr>
    <tr>
      <th scope="row">Comment</th>
      <td>
      {{ $client->comment }}
      </td>
    </tr>
      <th scope="row">I agree to receiving marketing and promotional materials.</th>
      <td>
         @if($client->agreement == '0')
        
        @else
        {{ $client->agreement }}
        @endif
      </td>
    </tr>
  </tbody>
</table>

 
    

        @endforeach
</div>



