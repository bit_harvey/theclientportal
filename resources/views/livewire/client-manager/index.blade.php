<div class="table-responsive">

<div class="p-2">
@if (session()->has('message'))
        <div class="alert alert-success">
        {{ session('message') }}
        </div>
@endif
@if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="alert alert-danger">
      {{ $error }}
    </div>
    @endforeach
@endif
    <div class="form-row">
        <div class="form-group col-md-3">
            <label><i class="fa fa-search"></i> Search </label>
            <input wire:model="search"  class="form-control" type="text" placeholder="Search" />
        </div>
        <div class="form-group col-md-3">
            <label><i class="fa fa-search"></i> Fields </label>
            <select wire:model="fields" class="form-control">
                <option value="application_registrations.fname">Firstname</option>
                <option value="application_registrations.lname">Lastname</option>
                <option value="application_registrations.email">Email</option>
                <option value="users.name">Agent</option>
                <option value="application_registrations.assignee">Assignee</option>
            </select>
        </div>
        <div class="col-md-3">
            <label>from: </label>
                <input wire:model="from" type="date" class="form-control">
            </div>
            <div class="col-md-3">
            <label>to: </label>
                <input  wire:model="to" type="date" class="form-control">
        </div>
    </div>
</div>
    <table class="card-table table">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Agent</th>
            <th scope="col">Assignee</th>
            <th scope="col">Status</th>
            <th scope="col">Created_at</th>
            @if(Auth::user()->role == "Agent")
            @else
            <th scope="col">Action</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($application_registrations as $application_registration)
        <tr>
            <td>{{ $application_registration->fname }} {{ $application_registration->lname }}</td>
            <td>{{ $application_registration->email }}</td>
            <td>{{ $application_registration->name }}</td>
            <td>{{ $application_registration->assignee }}</td>
            <td>
                @if ($application_registration->step_status  == 0)
                    <span class="badge badge-secondary">Pending</span>
                @elseif ($application_registration->step_status  == 1)
                    <span class="badge badge-danger">Disapproved</span>
                @elseif ($application_registration->step_status  == 2)
                    <span class="badge badge-success">Approve</span>
                @else ($application_registration->step_status  == 5)
                    <span class="badge badge-danger">Cancelled</span>
                @endif
            </td>
            @if(Auth::user()->role == "Agent")
            @else
            <td>
            {{ date('F j, Y, g:i', strtotime($application_registration->created_at)) }}</td>
            <td>
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('client-manager.view', $application_registration->id) }}">View</a>
                    <a class="dropdown-item" href="{{ route('client-manager.process', $application_registration->id) }}">Process</a>
                    <a class="dropdown-item" href="{{ route('client-manager.edit', $application_registration->id) }}">Edit</a>
                </div>
            </div>

                </div>
  
            </td>
            @endif
        </tr>
        @endforeach
        </tbody>
    </table>

    <hr>
   <div class="pagination ml-2">
    {{ $application_registrations->links('livewire.pagination.livewire-pagination')}}
    </div>
