<div>   

        <table class="table table-bordered">
        <thead>
            <tr>
            @foreach($application_registrations as $application_registration)
            <th>{{ $application_registration->fname }} {{$application_registration->lname}} ({{$application_registration->email}})</th> 
            @endforeach
            <th scope="col"></th>
            <th scope="col"><a class="btn btn-primary" wire:click="completeall()">Complete All</a></th>
            </tr>
        </thead>
        <tbody>
        <div>
       
            @foreach($process_steps as $process_step)   
            <tr>
                
                @if($process_step->step_id != '111')
                    <td style="padding-left:30px">{{ $process_step->process_name}}</td>
                @else 
                    <td><b>{{ $process_step->process_name}}</b></td>
                @endif
                    <td>
                        <small class="text-muted">
                                @if ($process_step->step_status  == NULL)
                                <span class="badge badge-secondary">Pending</span>
                                @elseif ($process_step->step_status  == 0)
                                     <span class="badge badge-secondary">Pending</span>
                                @elseif ($process_step->step_status == 1)
                                    <span class="badge badge-danger">On Progress</span>
                                @elseif ($process_step->step_status == 2)
                                    <span class="badge badge-success">Completed</span>
                                @elseif ($process_step->step_status == 3)
                                    <span class="badge badge-danger">Disapproved</span>
                                @elseif ($process_step->step_status == 4)
                                    <span class="badge badge-success">Approved</span>
                                @else ($process_step->step_status == 5)
                                    <span class="badge badge-danger">Cancelled</span>
                                @endif
                        </small>
                    
                    </td>
                    <td>
                        <div class="dropdown show">
                        <a class="btn btn-primary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            @if($process_step->application_id  == NULL)
                            <!-- <a class="dropdown-item" wire:click="onprogress({{ $process_step->process_step }})">On Progress</a> -->
                            @elseif($process_step->step_id == '10')
                            <a class="dropdown-item" wire:click="pending({{ $process_step->pending_id }})">Pending</a>
                            <a class="dropdown-item" wire:click="disapproved({{ $process_step->pending_id }})">Disapproved</a>
                            <a class="dropdown-item" wire:click="approved({{ $process_step->pending_id }})">Approved</a>
                            <a class="dropdown-item" wire:click="cancelled({{ $process_step->pending_id }})">Cancelled</a>
                            @else
                            <a class="dropdown-item" wire:click="pending({{ $process_step->pending_id }})">Pending</a>
                            <a class="dropdown-item" wire:click="updateonprogress({{ $process_step->pending_id }})">On Progress</a>
                            <a class="dropdown-item" wire:click="complete({{ $process_step->pending_id }})">Complete</a>
                            @endif
                        </div>
                    </div>
                        
                    </td>
            </tr>
            @endforeach
        </tbody>
        </table>

  