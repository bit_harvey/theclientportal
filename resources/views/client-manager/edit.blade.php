@extends('layouts.app-sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
             <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    @if( auth::user()->role == 'Client')
                    <li class="breadcrumb-item active">Profile</li>
                    @else 
                    <li class="breadcrumb-item active"><a href="{{route('client-manager')}}">Client Manager</a></li>
                    <li class="breadcrumb-item active">Edit Client</li>
                    @endif
                </ol>

                <div class="card">
                    <div class="card-body">
                        @if( auth::user()->role == 'Client')
                            <h5 class="card-title">Profile</h5><hr>
                        @else
                            <h5 class="card-title">Edit Client</h5><hr>
                        @endif
                        @livewire('client-manager.edit', ['edit_id' => $edit_id])
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection