@extends('layouts.app-sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ URL::previous() }}">Clients</a></li>
                <li class="breadcrumb-item active" aria-current="page">View Client</li>
            </ol>

                <div class="card">
                <div class="card-header bg-white">
                        <h5>View Client<a href="" type="button" class="btn btn-dark float-right">Other Files and Notes</a> <a href="{{route('client-manager.process', $view_id)}}" type="button" class="btn btn-dark float-right mr-2">Process</a></h5>
                    </div>
                    <div class="card-body">
                        @livewire('client-manager.view', ['view_id' => $view_id])
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection