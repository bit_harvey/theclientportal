@extends('layouts.app-sidebar')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('client-manager')}}">Client Manager</a></li>
                    <li class="breadcrumb-item active">Process</li>
                </ol>
                <div class="card">
                    <div class="card-header bg-white">
                        <h5 class="card-title">Process<a href="{{ route('client-manager.process', $process_id) }}" type="button" class="btn btn-dark float-right">Process</a><a href="{{ route('application-listing.view', $process_id) }}" type="button" class="btn btn-dark float-right mr-2">Listing</a></h5>
                    </div>
                    <div class="card-body">
                        @livewire('client-manager.process', ['process_id' => $process_id])
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection