@extends('layouts.app')

@section('content')
    @if (session()->has('message'))
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    
                        <div class="card">
                            <div class="card-header bg-dark">
                                <h1 class="card-title text-white">
                                    <i class="fa fa-check-circle"></i>
                                    Your application was successfully submitted.
                                </h1>
                                <p class="text-white">
                                We have already received your application. We will review your application and one of our representative will get in touch with you soon. In the meantime, you may want to check our <a href="https://www.thailandelitevisas.com/membership-overview/#membership-cards">membership cards</a> you can avail or review the 
                                <a href="https://www.thailandelitevisas.com/membership-overview/#privileges">membership privileges</a>. Thank You!
                                </p>
                            </div>
                                

                        </div>
                
        
                </div>
            </div>
        </div>
    @endif
@endsection