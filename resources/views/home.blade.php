@extends('layouts.app-sidebar')

@section('content')
<div class="container-fluid">


    <div class="row">
        
        <div class="col-md-12">
            <div class="card">
                @if(Auth::user()->role == "Admin")
                <div class="row">
                    <div class="col-md-12">
                            <div class="m-4">
                                <div class="row w-100">
                                        <div class="col-md-3 p-4">
                                            <div class="card border-info mx-sm-1 p-3">
                                                <div class="card border-info shadow text-info p-3 my-card" ><span class="fa fa-user" aria-hidden="true"></span></div>
                                                <div class="text-info text-center mt-3"><h4>Applicants</h4></div>
                                                <div class="text-info text-center mt-2"><h1>{{$applicants}}</h1></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-4">
                                            <div class="card border-success mx-sm-1 p-3">
                                                <div class="card border-success shadow text-success p-3 my-card"><span class="fa fa-check-circle" aria-hidden="true"></span></div>
                                                <div class="text-success text-center mt-3"><h4>Approved</h4></div>
                                                <div class="text-success text-center mt-2"><h1>{{$approved}}</h1></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-4">
                                            <div class="card border-danger mx-sm-1 p-3">
                                                <div class="card border-danger shadow text-danger p-3 my-card" ><span class="fa fa-ban" aria-hidden="true"></span></div>
                                                <div class="text-danger text-center mt-3"><h4>Disapproved</h4></div>
                                                <div class="text-danger text-center mt-2"><h1>{{$disapproved}}</h1></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-4">
                                            <div class="card border-secondary mx-sm-1 p-3">
                                                <div class="card border-secondary shadow text-secondary p-3 my-card" ><span class="fa fa-clock-o" aria-hidden="true"></span></div>
                                                <div class="text-secondary text-center mt-3"><h4>Pending</h4></div>
                                                <div class="text-secondary text-center mt-2"><h1>{{$pending}}</h1></div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        @endif
                        @if(Auth::user()->role == "Agent")
                <div class="row">
                    <div class="col-md-12">
                            <div class="m-4">
                                <div class="row w-100">
                                        <div class="col-md-3 p-4">
                                            <div class="card border-info mx-sm-1 p-3">
                                                <div class="card border-info shadow text-info p-3 my-card" ><span class="fa fa-user" aria-hidden="true"></span></div>
                                                <div class="text-info text-center mt-3"><h4>Applicants</h4></div>
                                                <div class="text-info text-center mt-2"><h1>234</h1></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-4">
                                            <div class="card border-success mx-sm-1 p-3">
                                                <div class="card border-success shadow text-muted p-3 my-card"><span class="fa fa-check-circle" aria-hidden="true"></span></div>
                                                <div class="text-muted text-center mt-3"><h4>Approved</h4></div>
                                                <div class="text-muted text-center mt-2"><h1>9332</h1></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-4">
                                            <div class="card border-danger mx-sm-1 p-3">
                                                <div class="card border-danger shadow text-danger p-3 my-card" ><span class="fa fa-ban" aria-hidden="true"></span></div>
                                                <div class="text-danger text-center mt-3"><h4>Disapproved</h4></div>
                                                <div class="text-danger text-center mt-2"><h1>346</h1></div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 p-4">
                                            <div class="card border-secondary mx-sm-1 p-3">
                                                <div class="card border-secondary shadow text-secondary p-3 my-card" ><span class="fa fa-clock-o" aria-hidden="true"></span></div>
                                                <div class="text-secondary text-center mt-3"><h4>Pending</h4></div>
                                                <div class="text-secondary text-center mt-2"><h1>346</h1></div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        @endif
                        @if(Auth::user()->role == "Client")
          
                <div class="timeline p-4">
                    <h2 class="font-weight-light text-center text-muted ">Process Timeline</h2>
                    <div class="text-center">
                        <i class="fa fa-check-circle"></i> Pending
                        <i class="fa fa-check-circle text-orange"></i> On Progress 
                        <i class="fa fa-check-circle text-success"></i> Completed 
                    </div>
                    
                        <div class="row">
                            <!-- timeline item 1 left dot -->
                            <div class="col-auto text-center flex-column d-none d-sm-flex">
                                <div class="row h-50">
                                    <div class="col">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                                @forelse($step1 as $step1)
                                    @if($step1->process_id == 11 and $step1->step_status == 1)
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-orange border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card border-orange">
                                    @elseif($step1->process_id == 11 and $step1->step_status == 2)
                                    <h5 class="m-2">
                                    <span class="badge badge-pill bg-success border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card border-success">
                                @else
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-light border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card">
                                @endif
                                @empty
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-light border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card">
                                @endforelse
                                    <div class="card-body">
                                    @forelse($one_step as $one_step)
                                        <div class="float-right text-muted">
                                            @if($one_step->process_id == 1)
                                                {{ date('F j, Y, g:i', strtotime($one_step->updated_at)) }}
                                            @else
                                            @endif
                                        </div>
                                       
                                            @if($one_step->process_id == 1 and $one_step->step_status == 1)
                                                <h4 class="card-title text-orange">1. Submit <i class="fa fa-check-circle text-orange"></i></h4>
                                            @elseif($one_step->process_id == 1 and $one_step->step_status == 2)
                                                <h4 class="card-title text-success">1. Submit <i class="fa fa-check-circle text-success"></i></h4>
                                            @else  
                                            <h4 class="card-title text-muted">1. Submit</h4>
                                            @endif
                                        @empty
                                         <h4 class="card-title text-muted">1. Submit</h4>
                                        @endforelse
                                        
                                      

                                        <p class="card-text">Submission of Application</p>
                                        @forelse($two_step as $two_step)
                                            @if($two_step->process_id == 2 and $two_step->step_status == 1)
                                                <h4 class="card-title text-orange">A. Passport copies <i class="fa fa-check-circle text-orange"></i></h4>  
                                            @elseif($two_step->process_id == 2 and $two_step->step_status == 2)
                                                <h4 class="card-title text-success">A. Passport copies <i class="fa fa-check-circle text-success"></i></h4>
                                            @else
                                                <h4 class="card-title text-muted">A. Passport copies</h4>
                                            @endif
                                        @empty
                                            <h4 class="card-title text-muted">A. Passport copies</h4>
                                        @endforelse    
                                            <div class="p-2 mb-2 text-muted">
                                        @forelse($three_step as $three_step)
                                            @if($three_step->process_id == 3 and $three_step->step_status == 1)
                                                <div class="text-orange">i. Color Copy of Full Front Page <i class="fa fa-check-circle text-orange"></i></div>
                                            @elseif($three_step->process_id == 3 and $three_step->step_status == 2)
                                                <div class="text-success">i. Color Copy of Full Front Page <i class="fa fa-check-circle text-success"></i></div>
                                            @else
                                                <div class="text-muted">i. Color Copy of Full Front Page </div>
                                            @endif
                                        @empty
                                            <div class="text-muted">i. Color Copy of Full Front Page </div>
                                        @endforelse

                                        @forelse($four_step as $four_step)
                                            @if($four_step->process_id == 4 and $four_step->step_status == 1)
                                                <div class="text-orange">ii. Latest Visa Stamp to Thailand <i class="fa fa-check-circle text-orange"></i></div>
                                            @elseif($four_step->process_id == 4 and $four_step->step_status == 2)
                                                <div class="text-success">ii. Latest Visa Stamp to Thailand <i class="fa fa-check-circle text-success"></i></div>
                                            @else  
                                                <div>ii. Latest Visa Stamp to Thailand</div>
                                            @endif
                                        @empty
                                        <div>ii. Latest Visa Stamp to Thailand</div>
                                        @endforelse
                                        @forelse($fifth_step as $fifth_step) 
                                            @if($fifth_step->process_id == 5 and $fifth_step->step_status == 1)    
                                                <div class="text-orange">iii. 3 Blank Pages <i class="fa fa-check-circle text-orange"></i></div>
                                            @elseif($fifth_step->process_id == 5 and $fifth_step->step_status == 2)
                                                <div class="text-success">iii. 3 Blank Pages <i class="fa fa-check-circle text-succes"></i></div>
                                                @else  
                                                <div>iii. 3 Blank Pages</div>
                                            @endif
                                            @empty
                                            <div>iii. 3 Blank Pages</div>
                                        @endforelse
                                            </div>
                                        

                                            @forelse($six_step as $six_step) 
                                                 @if($six_step->process_id == 6 and $six_step->step_status == 1)
                                                    <h5 class="card-title text-orange">B. Photo ID <i class="fa fa-check-circle text-orange"></i></h5>  
                                                @elseif($six_step->process_id == 6 and $six_step->step_status == 2)
                                                    <h5 class="card-title text-success">B. Photo ID <i class="fa fa-check-circle text-success"></i></h5>  
                                                    @else  
                                                    <h5 class="card-title text-muted">B. Photo ID</h5>
                                                @endif
                                                @empty
                                                <h5 class="card-title text-muted">B. Photo ID</h5>
                                            @endforelse
                                            
                                            @forelse($seven_step as $seven_step) 
                                                @if($seven_step->process_id == 7 and $seven_step->step_status == 1)
                                                    <h5 class="card-title text-orange">C. Application Form <i class="fa fa-check-circle text-orange"></i></h5> 
                                                @elseif($seven_step->process_id == 7 and $seven_step->step_status == 2)
                                                    <h5 class="card-title text-success">C. Application Form <i class="fa fa-check-circle text-success"></i></h5> 
                                                @else  
                                                    <h5 class="card-title text-muted">C. Application Form</h5>
                                                @endif
                                                @empty
                                                <h5 class="card-title text-muted">C. Application Form</h5>
                                            @endforelse

                                            <p class="card-tex">*For Married Couples:</p>

                                            @forelse($eight_step as $eight_step) 
                                                @if($eight_step->process_id == 9 and $eight_step->step_status == 1)
                                                    <h5 class="card-title text-orange">D. Copy of Marriage Certificate <i class="fa fa-check-circle text-orange"></i></h5>
                                                @elseif($eight_step->process_id == 9 and $eight_step->step_status == 2)
                                                    <h5 class="card-title text-success">D. Copy of Marriage Certificate <i class="fa fa-check-circle text-success"></i></h5>
                                                @else  
                                                    <h5 class="card-title text-muted">D. Copy of Marriage Certificate</h5>
                                                @endif
                                                @empty
                                                <h5 class="card-title text-muted">D. Copy of Marriage Certificate</h5>
                                            @endforelse

                                            <p class="card-text">*For Parent & Child:</p>

                                            @forelse($nine_step as $nine_step)
                                                @if($nine_step->process_id == 11 and $nine_step->step_status == 1)
                                                    <h5 class="card-title text-orange">E. Birth certificate of child/children <i class="fa fa-check-circle text-orange"></i></h5>
                                                @elseif($nine_step->process_id == 11 and $nine_step->step_status == 2)
                                                    <h5 class="card-title text-success">E. Birth certificate of child/children <i class="fa fa-check-circle text-success"></i></h5>
                                                @else  
                                                    <h5>E. Birth certificate of child/children</h5>
                                                @endif
                                                @empty
                                                <h5>E. Birth certificate of child/children</h5>
                                            @endforelse


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/row-->
                        <!-- timeline item 2 -->
                        <div class="row">
                            <div class="col-auto text-center flex-column d-none d-sm-flex">
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                                @forelse($step2 as $step2)
                                    @if($step2->process_id == 12 and $step2->step_status == 1)
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-orange border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card border-orange">
                                    @elseif($step2->process_id == 12 and $step2->step_status == 2)
                                    <h5 class="m-2">
                                    <span class="badge badge-pill bg-success border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card border-success">
                                @else
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-light border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card">
                                @endif
                                @empty
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-light border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card">
                                @endforelse
                                    <div class="card-body">
                                    @forelse($ten_step as $ten_step)     
                                        <div class="float-right text-muted">
                                            @if($ten_step->process_id == 12)
                                                {{ date('F j, Y, g:i', strtotime($ten_step->updated_at)) }}
                                            @else
                                            @endif
                                            </div>

                                        
                                            @if($ten_step->process_id == 12 and $ten_step->step_status == 1)
                                                <h4 class="card-title text-orange">2. Applied to Immigration <i class="fa fa-check-circle text-orange"></i> </h4>  
                                            @elseif($ten_step->process_id == 12 and $ten_step->step_status == 2)   
                                                 <h4 class="card-title text-success">2. Applied to Immigration <i class="fa fa-check-circle text-success"></i></h4>                                  
                                            @else  
                                                <h4 class="card-title text-muted">2. Applied to Immigration </h4>  
                                                @endif
                                                @empty
                                                <h4 class="card-title text-muted">2. Applied to Immigration </h4>  
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <!--/row-->
                        <!-- timeline item 4 -->
                        <div class="row">
                            <div class="col-auto text-center flex-column d-none d-sm-flex">
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                                @forelse($step3 as $step3)
                                    @if($step3->process_id == 13 and $step3->step_status == 1)
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-orange border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card border-orange">
                                    @elseif($step3->process_id == 13 and $step3->step_status == 2)
                                    <h5 class="m-2">
                                    <span class="badge badge-pill bg-success border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card border-success">
                                @elseif($step3->process_id == 13 and $step3->step_status == 3)
                                    <h5 class="m-2">
                                    <span class="badge badge-pill bg-danger border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card border-danger">
                                @else
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-light border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card">
                                @endif
                                @empty
                                <h5 class="m-2">
                                    <span class="badge badge-pill bg-light border">&nbsp;</span>
                                </h5>
                                <div class="row h-50">
                                    <div class="col border-right">&nbsp;</div>
                                    <div class="col">&nbsp;</div>
                                </div>
                            </div>
                            
                            <!-- timeline item 1 event content -->
                            <div class="col py-2">
                                <div class="card">
                                @endforelse
                                    <div class="card-body">
                                    @forelse($eleven_step as $eleven_step)
                                        <div class="float-right text-muted">
                                            @if($eleven_step->process_id == 13)
                                                {{ date('F j, Y, g:i', strtotime($eleven_step->updated_at)) }}
                                            @else
                                            @endif
                                        </div>

                                        
                                            @if($eleven_step->process_id == 13 and $eleven_step->step_status == 1)
                                                <h4 class="card-title text-orange">3. Status <i class="fa fa-check-circle text-orange"></i></h4>
                                            @elseif($eleven_step->process_id == 13 and $eleven_step->step_status == 2)   
                                                 <h4 class="card-title text-success">3. Status <i class="fa fa-check-circle text-success"></i></h4>                                  
                                            @else  
                                                <h4 class="card-title text-muted">3. Status</h4>  
                                            @endif
                                            @empty
                                                <h4 class="card-title text-muted">3. Status</h4>  
                                        @endforelse

                                        @forelse($twelve_step as $twelve_step)
                                            @if($twelve_step->process_id == 14 and $twelve_step->step_status == 3)
                                                <i><h5 class="card-title text-danger">Disapproved</h5><i>   
                                            @elseif($twelve_step->process_id == 14 and $twelve_step->step_status == 4)   
                                                 <h5 class="card-title text-success">Approved</h5>                                  
                                            @else  
                                                
                                                @endif

                                        @empty
                                            Approve or Disapprove
                                        @endforelse
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!--/row-->
                       
                        @else @endif
                    </div>
                </div>
            </div>
              
        </div>
    </div>
</div>



@endsection
