<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_registrations', function (Blueprint $table) {
            $table->id();
            $table->string('member_approval_id')->nullable();
            $table->string('fname');
            $table->string('lname');
            $table->string('email')->unique();
            $table->string('nationality');
            $table->string('embassy')->nullable();;
            $table->string('country_number');
            $table->string('phone_number');
            $table->string('location');
            $table->string('province')->nullable();
            $table->string('program');
            $table->string('residential_address');
            $table->string('current_visa');
            $table->string('agent')->nullable();
            $table->string('assignee')->nullable();
            $table->string('question1')->nullable();
            $table->string('question2');
            $table->string('question3')->nullable();
            $table->string('question4')->nullable();
            $table->string('question5')->nullable();
            $table->string('question6');
            $table->string('question7')->nullable();
            $table->string('question8')->nullable();
            $table->string('upload_application_form')->nullable();
            $table->longtext('upload_passport')->nullable();
            $table->longtext('upload_share_link')->nullable();
            $table->string('upload_photo')->nullable();
            $table->longtext('upload_marriage_certificate')->nullable();
            $table->longtext('upload_child_birth_certificate')->nullable();
            $table->longtext('upload_others')->nullable();
            $table->string('comment')->nullable();
            $table->string('agreement')->nullable();
            $table->tinyInteger('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_registrations');
    }
}
