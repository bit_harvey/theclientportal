<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_listings', function (Blueprint $table) {
            $table->id();
            $table->integer('application_id');
            $table->date('date_sent');
            $table->date('date_received');
            $table->date('date_applied')->nullable();
            $table->integer('payment_status');
            $table->integer('application_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_listings');
    }
}
