<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('process_steps')->insert([
            'step_id' => '111',
            'process_name' => 'Submit',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '1',
            'process_name' => 'a. Passport copies',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '2',
            'process_name' => 'i. Color Copy of Full Front Page',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '3',
            'process_name' => 'ii. Latest Visa Stamp to Thailand',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '4',
            'process_name' => 'iii. 3 Blank Pages',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '5',
            'process_name' => 'b. Photo ID',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '6',
            'process_name' => 'c. Application Form',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '112',
            'process_name' => '*For Married Couples:',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '7',
            'process_name' => 'd. Copy of Marriage Certificate',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '113',
            'process_name' => '*For Parent & Child:',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '8',
            'process_name' => 'e. Birth certificate of child/children',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '222',
            'process_name' => 'Applied to Immigration ',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '333',
            'process_name' => 'Status',
        ]);

        DB::table('process_steps')->insert([
            'step_id' => '10',
            'process_name' => 'a. Approved/Disapproved',
        ]);

    }
}
