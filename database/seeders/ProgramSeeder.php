<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
            'program_name' => '5 yrs. Elite Easy Access',
            'program_amount' => '1000',
        ]);

        DB::table('programs')->insert([
            'program_name' => '5 yrs. Elite Family Excursion(min 2 person)',
            'program_amount' => '1000',
        ]);

        DB::table('programs')->insert([
            'program_name' => '5 yrs. Elite Family Alternative',
            'program_amount' => '1000',
        ]);
        
        DB::table('programs')->insert([
            'program_name' => '10 yrs. Elite Previlege Access',
            'program_amount' => '1000',
        ]);

        DB::table('programs')->insert([
            'program_name' => '20 yrs. Elite Ultimate Privilege (Grand Individial)',
            'program_amount' => '1000',
        ]);

        DB::table('programs')->insert([
            'program_name' => '20 yrs. Elite Superiority Extension',
            'program_amount' => '1000',
        ]);

        DB::table('programs')->insert([
            'program_name' => '20 yrs. Elite Family Premium',
            'program_amount' => '1000',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'Undecided',
            'program_amount' => '1000',
        ]);

        DB::table('programs')->insert([
            'program_name' => 'Others',
            'program_amount' => '1000',
        ]);
    }   
}
