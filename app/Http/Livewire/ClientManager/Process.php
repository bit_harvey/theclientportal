<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;
use App\Models\ApplicationRegistration;
use App\Models\ProcessStep;
use App\Models\ProcessTimeline;
use DB;
class Process extends Component
{
    public $process_id;

    public function render()
    {
        $process = $this->process_id;
        $application_registrations = ApplicationRegistration::where('id', $process)->get();

        $completes= ProcessTimeline::where('application_id', $process)->get();

        $process_steps = 
        DB::table('process_steps')
            ->select('*', 'process_steps.id as process_step','process_timelines.id as pending_id')
            ->leftJoin('process_timelines', function($q) use ($process)
            {
                $q->on('process_steps.id', '=', 'process_timelines.process_id')
                    ->where('process_timelines.application_id', '=', "$process");
            })
            ->leftJoin('application_registrations', 'application_registrations.id', '=', 'process_timelines.application_id') 
            ->orderBy('process_steps.id')
            ->get();
        return view('livewire.client-manager.process', compact('application_registrations', 'process_steps', 'completes'));
    }

    public function mount($process_id)
    {

    }

    public function pending($id)
    {
        $pending = ProcessTimeline::find($id);
        $pending->step_status = '0';
        $pending->save();
    }

    public function onprogress($id)
    {
        $process = $this->process_id;
        $onprogress = new ProcessTimeline;
        $onprogress->application_id = $process;
        $onprogress->process_id = $id;
        $onprogress->step_status = 1;
        $onprogress->save();

    }

    public function completeall()
    {
        ProcessTimeline::upsert([
            ['application_id' => $this->process_id, 'process_id' => 1, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 2, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 3, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 4, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 5, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 6, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 7, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 8, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 9, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 10, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 11, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 12, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 13, 'step_status' => 2],
            ['application_id' => $this->process_id, 'process_id' => 14, 'step_status' => 2]
        ], ['application_id', 'process_id'], ['step_status']);
    }

    public function complete($id)
    {
        $complete = ProcessTimeline::find($id);
        $complete->step_status = '2';
        $complete->save();

    }


    public function updateonprogress($id)
    {
        $updateonprogress = ProcessTimeline::find($id);
        $updateonprogress->step_status = '1';
        $updateonprogress->save();

    }

    public function approved($id)
    {
        $complete = ProcessTimeline::find($id);
        $complete->step_status = '4';
        $complete->save();

    }

    public function cancelled($id)
    {
        $complete = ProcessTimeline::find($id);
        $complete->step_status = '5';
        $complete->save();

    }

    public function disapproved($id)
    {
        $complete = ProcessTimeline::find($id);
        $complete->step_status = '3';
        $complete->save();

    }
}
