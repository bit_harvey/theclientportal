<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;
use App\Models\ApplicationRegistration;
use App\Models\User;
class Edit extends Component
{
    Public $edit_id,$fname, $lname, $member_approval_id, $email, $nationality, $embassy, $country_number, $phone_number, 
    $location, $province, $program, $residential_address, $current_visa, $agent, $assignee,
    $question1,$question2,$question3,$question4,$question5,$question6,$question7,$question8;

    public function render()
    {   
        $edit_id =$this->edit_id;
        $app_regs = ApplicationRegistration::where('id',)->first();
        $select_agents = User::where('role', 'Agent')->get();
        return view('livewire.client-manager.edit', compact('select_agents','app_regs'));
    }

    public function mount($edit_id)
    {

        $application_registrations = ApplicationRegistration::where('id',$edit_id)->first();
        $question6_decode = json_decode($this->question6);
        $this->member_approval_id = $application_registrations->member_approval_id;
        $this->fname = $application_registrations->fname;
        $this->lname = $application_registrations->lname;
        $this->email = $application_registrations->email;
        $this->nationality = $application_registrations->nationality;
        $this->embassy = $application_registrations->embassy;
        $this->country_number = $application_registrations->country_number;
        $this->phone_number = $application_registrations->phone_number;
        $this->location = $application_registrations->location;
        $this->province = $application_registrations->province;
        $this->program = $application_registrations->program;
        $this->residential_address = $application_registrations->residential_address;
        $this->current_visa = $application_registrations->current_visa;
        $this->agent = $application_registrations->agent;
        $this->assignee = $application_registrations->assignee;
        $this->question1 = $application_registrations->question1;
        $this->question2 = $application_registrations->question2;
        $this->question3 = $application_registrations->question3;
        $this->question4 = $application_registrations->question4;
        $this->question5 = $application_registrations->question5;
        $this->question6 = $application_registrations->question6_decode;
        $this->question7 = $application_registrations->question7;
        $this->question8 = $application_registrations->question8;

    }

    public function update()
    {
         $this->validate([
        'member_approval_id' => 'nullable',
        'fname' => 'required|min:3',
        'lname' => 'required|min:2',
        'email' => 'required',
        'nationality' => 'required',
        'embassy' => 'nullable',
        'country_number' => 'required',
        'phone_number' => 'required',
        'location' => 'required',
        'province' => 'nullable',
        'program' => 'required',
        'residential_address' => 'required',
        'current_visa' => 'required',
        'question1' => 'nullable',
        'question2' => 'required',
        'question3' => 'nullable',
        'question4' => 'nullable',
        'question5' => 'nullable',
        'question6' => 'required',
        'question7' => 'nullable',
        'question8' => 'nullable'

        ]);
            
        $application_registrations = [
        'member_approval_id' =>  $this->member_approval_id,
        'fname' => $this->fname,
        'lname' => $this->lname,
        'email' => $this->email,
        'nationality' => $this->nationality,
        'embassy' => $this->embassy,
        'country_number' => $this->country_number,
        'phone_number' => $this->phone_number,
        'location' => $this->location,
        'province' => $this->province,
        'program' => $this->program,
        'residential_address' => $this->residential_address,
        'current_visa' => $this->current_visa,
        'agent' => $this->agent,
        'assignee' => $this->assignee,
        'question1' => $this->question1,
        'question2' => $this->question2,
        'question3' => $this->question3,
        'question4' => $this->question4,
        'question5' => $this->question5,
        'question6' => $this->question6,
        'question7' => $this->question7,
        'question8' => $this->question8

        ];
        

        ApplicationRegistration::find($this->edit_id)->update($application_registrations);

        session()->flash('message', 'Client Updated Successfully.');


    }
}
