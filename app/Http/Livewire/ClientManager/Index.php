<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;
use Livewire\WithPagination; 
use Illuminate\Pagination\Paginator;
use App\Models\ApplicationRegistration;
use DB;
use Auth;

class Index extends Component
{

    use WithPagination;
    public $search;
    public $fields;
    public $from;
    public $to;
    public $currentPage = 1;

    public function render()
    {
        if($this->fields == ""){
            $fields = "application_registrations.fname";
        }else{
            $fields = $this->fields;
        };

        if($this->from == "" and $this->to == "" ){
            $from = "0000-00-00";
            $to= "2021-12-30";
        }else{
            $from = $this->from;
            $to= $this->to;
        };

        $client_status="14";
        if(Auth::user()->role == 'Admin'){
            $application_registrations = ApplicationRegistration::where('status', '=', 1)
            ->leftJoin('users', 'users.id', '=', 'application_registrations.agent')
            ->leftJoin('process_timelines', function($q) use ($client_status)
            {
                $q->on('application_registrations.id', '=', 'process_timelines.application_id')
                    ->where('process_timelines.process_id', '=', "$client_status");
            })
            ->where(function($query) use ($fields){
                $query->where($fields,'like', '%'.$this->search.'%');
            })
            ->where(function($query) use ($from, $to, $fields){
                $query->whereBetween('application_registrations.created_at', [$from.'%', $to.'%']);
            })
            ->orderBy('application_registrations.id', 'DESC')
            ->select('application_registrations.id','application_registrations.email','application_registrations.fname','application_registrations.lname','application_registrations.email','users.name','process_timelines.step_status','application_registrations.assignee','application_registrations.created_at')
            ->paginate(10);
        }else{
            $application_registrations = ApplicationRegistration::where('status', '=', 1)
            ->leftJoin('users', 'users.id', '=', 'application_registrations.agent')
            ->leftJoin('process_timelines', function($q) use ($client_status)
            {
                $q->on('application_registrations.id', '=', 'process_timelines.application_id')
                    ->where('process_timelines.process_id', '=', "$client_status");
            })
            ->where(function($query) use ($fields){
                $query->where($fields,'like', '%'.$this->search.'%');
            })
            ->where(function($query) use ($from, $to, $fields){
                $query->whereBetween('application_registrations.created_at', [$from.'%', $to.'%']);
            })
            ->where('application_registrations.agent', Auth::user()->id)
            ->orderBy('application_registrations.id', 'DESC')
            ->select('application_registrations.id','application_registrations.email','application_registrations.fname','application_registrations.lname','application_registrations.email','users.name','process_timelines.step_status','application_registrations.assignee','application_registrations.created_at')
            ->paginate(10);
        }
        return view('livewire.client-manager.index', compact('application_registrations'));
    }

    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }
}
