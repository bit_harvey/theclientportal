<?php

namespace App\Http\Livewire\ClientManager;

use Livewire\Component;
use App\Models\ApplicationRegistration;

class View extends Component
{
    Public $view_id;

    public function render()
    {
        $view = $this->view_id;
        $clients = ApplicationRegistration::where('id', $view)->get();
        return view('livewire.client-manager.view', compact('clients'));
    }

    public function mount($view_id)
    {

    }

}
