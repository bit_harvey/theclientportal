<?php

namespace App\Http\Livewire\Announcement;

use Livewire\Component;
use App\Models\Announcement;

class View extends Component
{
    public $view_id, $title, $description;

    public function render()
    {
        return view('livewire.announcement.view');
    }

    public function mount($view_id){
        
        $announcements = Announcement::where('id',$view_id)->first();
        $this->title = $announcements->title;
        $this->description = $announcements->description;
    }

    public function update()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required'
        ]);
        $announcements = [
            'title' =>  $this->title,
            'description' =>  $this->description,
             ];

             Announcement::find($this->view_id)->update($announcements);

             session()->flash('message', 'Announcement Updated Successfully.');
    }
}
