<?php

namespace App\Http\Livewire\Announcement;

use Livewire\Component;
use App\Models\Announcement;
class Add extends Component
{
    public $title, $description;

    public function render()
    {
        return view('livewire.announcement.add');
    }

    private function resetInputFields(){
        $this->title =  '';
        $this->description =  '';
    }
    
    public function add()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        Announcement::Create([
            'title' => $this->title,
            'description' => $this->description,
        ]);

        session()->flash('message', 'Announcement Added Successfully.');
        $this->resetInputFields();
    }
}
