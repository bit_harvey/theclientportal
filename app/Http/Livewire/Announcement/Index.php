<?php

namespace App\Http\Livewire\Announcement;

use Livewire\Component;
use App\Models\Announcement;
use Livewire\WithPagination; 
use Illuminate\Pagination\Paginator;

class Index extends Component
{
    use WithPagination;

    public $currentPage = 1;

    public function render()
    {
        $announcements = Announcement::paginate(10);
        return view('livewire.announcement.index', compact('announcements'));
    }

    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }
}
