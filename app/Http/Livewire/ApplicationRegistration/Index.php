<?php

namespace App\Http\Livewire\ApplicationRegistration;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Livewire\WithPagination; 
use Illuminate\Pagination\Paginator;

use App\Models\ApplicationRegistration;
use App\Models\ApplicationListing;
use App\Models\User;
use Carbon\Carbon;
use Auth;
use DB;

class Index extends Component
{
    public $client_id,$name,$email,$role,$active,$password,$agent, $date_sent,$date_received,$fields, $status, $from, $to;
    public $updateMode = false;

    use WithPagination;
    public $search;

    public $currentPage = 1;

    public function render(){
        $sub_agents = User::where('role', 'Agent')->get();

        if($this->fields == ""){
            $fields = "application_registrations.fname";
        }else{
            $fields = $this->fields;
        };

        
        if($this->from == "" and $this->to == "" ){
            $from = "0000-00-00";
            $to= "2021-12-30";
        }else{
            $from = $this->from;
            $to= $this->to;
        };


        if($this->status == ""){
            $status = "0";
        }else{
            $status = $this->status;
        };

        if(Auth::user()->role == 'Admin'){
            $application_registrations = ApplicationRegistration::where('application_registrations.status' , '!=', 1)
            ->leftJoin('users', 'users.id', '=', 'application_registrations.agent')
            ->where(function($query) use ($fields,$from,$to){
                $query->where($fields,'like', '%'.$this->search.'%');
            })->where(function($query) use ($status){
                $query->where('application_registrations.status', $status);
            })->where(function($query) use ($from, $to, $fields){
                $query->whereBetween('application_registrations.created_at', [$from.'%', $to.'%']);
            })
            ->orderBy('application_registrations.id', 'DESC')
            ->select('application_registrations.id','application_registrations.email','application_registrations.fname','application_registrations.lname','application_registrations.email','users.name','application_registrations.status','application_registrations.created_at')        
            ->paginate(10);
        }else{
            $application_registrations = ApplicationRegistration::where('application_registrations.status', '=', 0)
            ->leftJoin('users', 'users.id', '=', 'application_registrations.agent')
            ->where(function($query) use ($fields,$from,$to){
                $query->where($fields,'like', '%'.$this->search.'%');
            })->where(function($query) use ($status){
                $query->where('application_registrations.status', $status);
            })->where(function($query) use ($from, $to, $fields){
                $query->whereBetween('application_registrations.created_at', [$from.'%', $to.'%']);
            })
            ->orderBy('application_registrations.id', 'DESC')
            ->where('application_registrations.agent', Auth::user()->id)
            ->select('application_registrations.id','application_registrations.email','application_registrations.fname','application_registrations.lname','application_registrations.email','users.name','application_registrations.status','application_registrations.created_at')        
            ->paginate(10);

        }

        return view('livewire.application-registration.index', compact('application_registrations', 'sub_agents'));
    }

    private function resetInputFields(){
        $this->client_id = '';
        $this->name =  '';
        $this->email =  '';
        $this->active =  '';
        $this->role =  '';
        $this->password =  '';
    }


    public function approved($id)
    {
        $random =Str::random(9);
        $this->updateMode = true;
        $applicant = ApplicationRegistration::where('id',$id)->first();
        $this->client_id = $id;
        $this->name = $applicant->fname;
        $this->email = $applicant->email;
        $this->role = 'Client';
        $this->active = '1';
        $this->password = $random; 
    }

    public function assign($id)
    {
        $this->client_id = $id;
    }

    public function assign_store()
    {

    $this->validate([
        'client_id' => 'required',
        'agent' => 'required',
    ]);

    $assign = ApplicationRegistration::find($this->client_id,);
    $assign->agent = $this->agent;
    $assign->save();
        
    session()->flash('message', 'Assign Agent Successfully.');
    $this->resetInputFields();
    $this->emit('userStore'); 
    }

    

    public function approved_store()
    {

        DB::transaction(function() {
            $this->validate([
                    'client_id' => 'required',
                    'name' => 'required',
                    'email' => 'required|unique:users,email',
                    'role' => 'required',
                    'active' => 'required',
                    'password' => 'required',
                ]);

            
                User::Create([
                    'client_id' => $this->client_id,
                    'name' => $this->name,
                    'email' => $this->email,
                    'role' => $this->role,
                    'active' => $this->active,
                    'password' => Hash::make($this->password),
                ]);

                
                    $status = ApplicationRegistration::find($this->client_id,);
                    $status->status = '1';
                    $status->assignee = Auth::user()->email;
                    $status->save();


                    $this->validate([
                        'client_id' => 'required',
                    ]);

                    $date_sent =  date('Y-m-d');
                    $date_received =  date('Y-m-d'); 

                    ApplicationListing::Create([
                        'application_id' => $this->client_id,
                        'date_sent' => $date_sent,
                        'date_received' => $date_received,
                        'payment_status' => '0',
                        'application_status' => '0',
                    ]);

                    $details = [
                        'email' => $this->email,
                        'password' => $this->password,
                    ];

                    \Mail::to($this->email)->send(new \App\Mail\UserCredential($details));
          
        });

        session()->flash('message', 'Application Approved Successfully.');
        $this->resetInputFields();
        $this->emit('userStore'); 
    }

    public function decline($id)
    {

        $status = ApplicationRegistration::find($id);
        $status->status = '2';
        $status->save();
        session()->flash('message', 'Decline Successfully.');
    }


    public function restore($id)
    {

        $status = ApplicationRegistration::find($id);
        $status->status = '0';
        $status->save();
        session()->flash('message', 'Restore Successfully.');
    }
    
    public function cancel()
    {
        $this->updateMode = false;
    }

    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }

    
}


