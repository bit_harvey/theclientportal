<?php

namespace App\Http\Livewire\ApplicationRegistration;

use Livewire\Component;
use Livewire\WithFileUploads;
use App\Models\ApplicationRegistration;
use App\Models\User;
use App\Models\Program;


class Add extends Component
{
    use WithFileUploads;
    
    public function render()
    {
        $programs = Program::all();
        $select_agents = User::where('role', 'Agent')->get();
        return view('livewire.application-registration.add', compact('select_agents', 'programs'));
    }

    
    public $fname;
    public $lname;
    public $email;
    public $nationality = 'N/A';
    public $country_number = 'N/A';
    public $phone_number = '0';
    public $location = 'N/A';
    public $province;
    public $program = 'N/A';
    public $residential_address = 'N/A';
    public $current_visa = 'N/A';  
    public $question1;    
    public $question2 = 'N/A'; 
    public $question3; 
    public $question4; 
    public $question5; 
    public $question6= []; 
    public $question7; 
    public $question8;  
    public $upload_application_form = []; 
    public $upload_passport = []; 
    public $upload_share_link; 
    public $upload_photo = []; 
    public $upload_marriage_certificate = [];
    public $upload_child_birth_certificate = [];  
    public $upload_others = [];  
    public $comment;  
    public $agreement; 
    public $agent; 

    private function resetInputFields(){
        $this->fname = '';
        $this->lname = '';
        $this->email = '';
        $this->nationality = '';
        $this->country_number = '';
        $this->phone_number = '';
        $this->location = '';
        $this->province = '';
        $this->fname = '';
        $this->program = '';
        $this->residential_address = '';
        $this->current_visa = '';
        $this->question1 = '';
        $this->question2 = '';
        $this->question3 = '';
        $this->question4 = '';
        $this->question5 = '';
        $this->question6 = '';
        $this->question7 = '';
        $this->question8 = '';
        $this->upload_application_form = '';
        $this->upload_passport = '';
        $this->upload_share_link = '';
        $this->upload_photo = '';
        $this->upload_marriage_certificate = '';
        $this->upload_child_birth_certificate = '';
        $this->upload_others = '';
        $this->comment = '';
        $this->agreement = '';
        $this->agent = '';
        
    }
    protected $rules = [
        'fname' => 'required|min:3',
        'lname' => 'required|min:2',
        'email' => 'required|email|unique:application_registrations|max:255',
        'nationality' => 'required',
        'country_number' => 'required',
        'phone_number' => 'required',
        'location' => 'required',
        'province' => 'nullable',
        'program' => 'required',
        'residential_address' => 'required',
        'current_visa' => 'required',
        'question1' => 'nullable',
        'question2' => 'required',
        'question3' => 'nullable',
        'question4' => 'nullable',
        'question5' => 'nullable',
        'question6.*' => 'required',
        'question7' => 'nullable',
        'question8' => 'nullable',
        'upload_application_form.*' => 'required|mimes:pdf|max:8048',
        'upload_passport.*' => 'image|max:4000',
        'upload_share_link' => 'nullable',
        'upload_photo.*' => 'required',
        'upload_marriage_certificate.*' => 'image|max:4000',
        'upload_child_birth_certificate.*' => 'image|max:4000',
        'upload_others.*' =>'image|max:4000',
        'comment' => 'nullable',
        'agreement' => 'nullable',
        'agent' => 'nullable',


        
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function saveApplication()
    {
        $validatedData = $this->validate();

        foreach ($this->upload_application_form as $key => $upload_application) {
            $this->upload_application_form[$key] = $upload_application->store('images','public');
        }
        $this->upload_application_form = json_encode($this->upload_application_form);

        foreach ($this->upload_passport as $key => $passport) {
            $this->upload_passport[$key] = $passport->store('images','public');
        }
        $this->upload_passport = json_encode($this->upload_passport);

        foreach ($this->upload_photo as $key => $photo) {
            $this->upload_photo[$key] = $photo->store('images','public');
        }
        $this->upload_photo = json_encode($this->upload_photo);


        foreach ($this->upload_marriage_certificate as $key => $marriage_certificate) {
            $this->upload_marriage_certificate[$key] = $marriage_certificate->store('images','public');
        }
        $this->upload_marriage_certificate = json_encode($this->upload_marriage_certificate);

        foreach ($this->upload_child_birth_certificate as $key => $child_birth_certificate) {
            $this->upload_child_birth_certificate[$key] = $child_birth_certificate->store('images','public');
        }
        $this->upload_child_birth_certificate = json_encode($this->upload_child_birth_certificate);

        foreach ($this->upload_others as $key => $others) {
            $this->upload_others[$key] = $others->store('images','public');
        }
        $this->upload_others = json_encode($this->upload_others);
 
        $question6 = json_encode($this->question6);

        ApplicationRegistration::create([
            'fname' => $this->fname,
            'lname' => $this->lname,
            'email' => $this->email,
            'nationality' =>  $this->nationality,
            'country_number' => $this->country_number,
            'phone_number' => $this->phone_number,
            'location' => $this->location,
            'province' => $this->province,
            'program' => $this->program,
            'residential_address' => $this->residential_address,
            'current_visa' => $this->current_visa,
            'question1' => $this->question1,
            'question2' => $this->question2,
            'question3' => $this->question3,
            'question4' => $this->question4,
            'question5' => $this->question5,
            'question6' => $question6,
            'question7' => $this->question7,
            'question8' => $this->question8,
            'upload_application_form' => $this->upload_application_form,
            'upload_passport' => $this->upload_passport,
            'upload_share_link' => $this->upload_share_link,
            'upload_photo' => $this->upload_photo,
            'upload_marriage_certificate' => $this->upload_marriage_certificate,
            'upload_child_birth_certificate' => $this->upload_child_birth_certificate,
            'upload_others' => $this->upload_others,
            'comment' => $this->comment,
            'agreement' => $this->agreement,
            'agent' => $this->agent,
            'status' => '0',

        ]);


        session()->flash('message', 'Application submitted successfully.');

        $this->resetInputFields();
        
        return redirect()->to('/application-registration');
    }
}
