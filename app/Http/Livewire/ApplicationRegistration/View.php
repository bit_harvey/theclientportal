<?php

namespace App\Http\Livewire\ApplicationRegistration;

use Livewire\Component;
use App\Models\ApplicationRegistration;

class View extends Component
{
    Public $view_id;

    public function render()
    {
        $view = $this->view_id;
        $application_registrations = ApplicationRegistration::where('id', $view)->get();
        return view('livewire.application-registration.view', compact('application_registrations'));
    }

    public function mount($view_id)
    {

    }
}
