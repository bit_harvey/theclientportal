<?php

namespace App\Http\Livewire\ApplicationListing;

use Livewire\Component;
use Livewire\WithPagination; 
use Illuminate\Pagination\Paginator;
use App\Models\ApplicationListing;
use App\Models\ApplicationRegistration;
use DB;
class Index extends Component
{

    use WithPagination;

    public $search;

    public $sentfrom;
    public $sentto;
    public $fields;



    public function render()
    {
       
        if($this->sentfrom == "" and $this->sentto == "" ){
            $sentfrom = "0000-00-00";
            $sentto= "2021-12-30";
        }else{
            $sentfrom = $this->sentfrom;
            $sentto= $this->sentto;
        };

        if($this->fields == ""){
            $fields = "application_listings.date_sent";
        }else{
            $fields = $this->fields;
        };
        
            $application_listings = ApplicationListing::Join('application_registrations', 'application_registrations.id', '=', 'application_listings.application_id')
            ->where(function($query){
                $query->where('application_registrations.lname','like', '%'.$this->search.'%')
                ->orWhere('application_registrations.fname','like', '%'.$this->search.'%')
                ->orwhere('application_registrations.email','like', '%'.$this->search.'%');
            })->where(function($query) use ($sentfrom, $sentto, $fields){
                $query->whereBetween($fields, [$sentfrom.'%', $sentto.'%']);
            })
            ->orderBy('application_registrations.id', 'DESC')
            ->select('*','application_listings.id as app_id',)
            ->paginate(10);
        return view('livewire.application-listing.index', compact('application_listings'));
    }

    public function onprogress($id)
    {
        $onprogress = ApplicationListing::find($id);
        $onprogress->application_status = '0';
        $onprogress->save();
        session()->flash('message', 'This application is on progress.');
    }

    public function approved($id)
    {
        $approved = ApplicationListing::find($id);
        $approved->application_status = '1';
        $approved->save();
        session()->flash('message', 'Approved Application Successfully.');
    }

    public function disapproved($id)
    {
        $disapproved = ApplicationListing::find($id);
        $disapproved->application_status = '2';
        $disapproved->save();
        session()->flash('message', 'Disapproved Application Successfully.');
    }


    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }
}
