<?php

namespace App\Http\Livewire\ApplicationListing;

use Livewire\Component;
use App\Models\ApplicationListing;

class View extends Component
{
    Public $view_id,$date_sent,$date_received,$date_applied,$application_status,$payment_status;

    public function render()
    {

        $view = $this->view_id;
        $application_listings = ApplicationListing::Join('application_registrations', 'application_registrations.id', '=', 'application_listings.application_id')
        ->where('application_listings.application_id', $view)
        ->get();
        return view('livewire.application-listing.view', compact('application_listings'));
    }

    public function mount($view_id)
    {
        $application_listings = ApplicationListing::where('application_id',$view_id)->first();
        $this->date_sent = $application_listings->date_sent;
        $this->date_received = $application_listings->date_received;
        $this->date_applied = $application_listings->date_applied;
        $this->payment_status = $application_listings->payment_status;
        $this->application_status = $application_listings->application_status;
    }

    public function update()
    {
         $this->validate([
            'date_sent' => 'required',
            'date_received' => 'required',
            'date_applied' => 'nullable',
            'payment_status' => 'required',
            'application_status' => 'required'

        ]);
            
        $application_listings = [
        'date_sent' =>  $this->date_sent,
        'date_received' =>  $this->date_received,
        'date_applied' =>  $this->date_applied,
        'payment_status' => $this->payment_status,
        'application_status' => $this->application_status
         ];
        
        ApplicationListing::where('application_id',$this->view_id)->update($application_listings);

        session()->flash('message', 'Application Listing Updated Successfully.');

    }
}
