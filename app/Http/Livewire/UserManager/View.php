<?php

namespace App\Http\Livewire\UserManager;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Models\User;

class View extends Component
{

    public $view_id, $name, $email, $role, $active, $password; 

    public function render()
    {
        $view = $this->view_id;
        $user_managers = User::where('id', $view)
        ->get();

        return view('livewire.user-manager.view', compact('user_managers'));
    }

    public function mount($view_id)
    {
        $user_managers = User::where('id',$view_id)->first();
        $this->name = $user_managers->name;
        $this->email = $user_managers->email;
        $this->role = $user_managers->role;
        $this->active = $user_managers->active;
        $this->password = $user_managers->password;
    }
    
    public function update()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required',
            'role' => 'required',
            'active' => 'required',
        ]);
        $user_managers = [
            'name' =>   $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'active' => $this->active,
             ];

             User::find($this->view_id)->update($user_managers);

             session()->flash('message', 'User Updated Successfully.');
    }


    public function updatePassword()
    {
        $this->validate([
            'password' => 'required'
        ]);
        $user_managers = [
           
            'password' =>  Hash::make($this->password),
             ];

             User::find($this->view_id)->update($user_managers);

             session()->flash('password', 'Password Updated Successfully.');
    }

    
}
