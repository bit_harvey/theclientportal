<?php

namespace App\Http\Livewire\UserManager;

use Livewire\Component;
use Livewire\WithPagination; 
use Illuminate\Pagination\Paginator;
use App\Models\User;
class Index extends Component
{
    use WithPagination;
    public $search;
    public $role;

    public $currentPage = 1;

    public function render()
    {
        if($this->role == ""){
            $role = "Admin";
        }else{
            $role = $this->role;
        };

        $users = User::where(function($query){
            $query->where('name','like', '%'.$this->search.'%')
            ->orWhere('email','like', '%'.$this->search.'%');
        })->where(function($query) use($role){
            $query->where('role', $role);
        })
        ->orderBy('users.id', 'DESC')
        ->paginate(10);
        return view('livewire.user-manager.index', ['users'=> $users]);

    }
    public function setPage($url)
    {
        $this->currentPage = explode('page=', $url)[1];
        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }
}
