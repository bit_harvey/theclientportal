<?php

namespace App\Http\Livewire\UserManager;

use Livewire\Component;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class Add extends Component
{
    public $name, $email, $role, $active, $password;
    
    public function render()
    {
        return view('livewire.user-manager.add');
    }

    private function resetInputFields(){
        $this->name =  '';
        $this->email =  '';
        $this->active =  '';
        $this->role =  '';
        $this->password =  '';
    }
    
    public function add()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'role' => 'required',
            'active' => 'required',
            'password' => 'required'
        ]);

        User::Create([
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'active' => $this->active,
            'password' => Hash::make($this->password),
        ]);

        session()->flash('message', 'User Added Successfully.');
        $this->resetInputFields();
    }
    
}
