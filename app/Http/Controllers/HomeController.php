<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ProcessTimeline;
use App\Models\ApplicationRegistration;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $applicants = ApplicationRegistration::where('application_registrations.status', 0)
        ->count();

        $approved = ApplicationRegistration::where('application_registrations.status', 1)
        ->count();

        $disapproved = ApplicationRegistration::where('application_registrations.status', 2)
        ->count();

        $pending = ApplicationRegistration::where('application_registrations.status', 0)
        ->count();  

        $one_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 1)
        ->get();
        
        $two_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 2)
        ->get();

        $three_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 3)
        ->get();

        $four_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 4)
        ->get();

        $fifth_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 5)
        ->get();

        $six_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 6)
        ->get();

        $seven_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 7)
        ->get();

        $eight_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 9)
        ->get();

        $nine_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 11)
        ->get();

        $ten_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 12)
        ->get();

        $eleven_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 13)
        ->get();

        $twelve_step = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 14)
        ->get();

        $step1 = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 11)
        ->get();

        $step2 = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 12)
        ->get();

        $step3 = ProcessTimeline::where('process_timelines.application_id', Auth::user()->client_id)
        ->where('process_timelines.process_id', 13)
        ->get();

        return view('home', compact('applicants','approved','disapproved','pending','one_step', 'two_step', 'three_step', 'four_step', 'fifth_step', 'six_step', 'seven_step', 'eight_step', 'nine_step', 'ten_step', 'eleven_step', 'twelve_step', 'step1', 'step2', 'step3'));
    }
}
