<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationRegistrationController extends Controller
{
    public function index(){
        return view('application-registration.index');
    }


    public function view($view_id){
        $view_id = $view_id;
        return view('application-registration.view', compact('view_id'));
    }

    public function add(){
        return view('application-registration.add');
    }
    
}
