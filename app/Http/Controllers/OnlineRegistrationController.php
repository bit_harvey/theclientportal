<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OnlineRegistrationController extends Controller
{
    public function index(){

        return view('online-registration');
    }

    public function success(){

        return view('alert.success');
    }
}
