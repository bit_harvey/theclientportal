<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientManagerController extends Controller
{
    public function index(){

        return view('client-manager.index');
    }

    public function view($view_id){

        return view('client-manager.view', compact('view_id'));
    }

    public function edit($edit_id){

        return view('client-manager.edit', compact('edit_id'));
    }

    public function process($process_id){

        return view('client-manager.process', compact('process_id'));
    }
}
