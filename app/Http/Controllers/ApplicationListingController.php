<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApplicationListingController extends Controller
{
    public function index(){
        return view('application-listing.index');
    }

    public function view($view_id){

        return view('application-listing.view', compact('view_id'));
    }
}
