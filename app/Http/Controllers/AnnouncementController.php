<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnnouncementController extends Controller
{
    public function index(){
        return view('announcement.index');
    }

    public function add(){
        return view('announcement.add');
    }

    public function view($view_id){
        return view('announcement.view', compact('view_id'));
    }
}
