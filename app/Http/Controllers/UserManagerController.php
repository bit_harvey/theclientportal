<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserManagerController extends Controller
{
    public function index(){
        
        return view('user-manager.index');
    }

    public function add(){
        return view('user-manager.add');
    }

    public function view($view_id){
        return view('user-manager.view', compact('view_id'));
    }
}
