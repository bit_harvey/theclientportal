<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationListing extends Model
{
    use HasFactory;

    protected $fillable = [
        'application_id',
        'date_sent',
        'date_received',
        'payment_status',
        'application_status'
        
    ];
}
