<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProcessTimeline extends Model
{
    use HasFactory;

    protected $fillable = [
        'application_id',
        'process_id',
        'step_status'
        
    ];
}
