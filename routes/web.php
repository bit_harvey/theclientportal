<?php

use Illuminate\Support\Facades\Route;

use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\ClientMiddleware;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/online-registration', [App\Http\Controllers\OnlineRegistrationController::class, 'index'])->name('online-registration');
Route::get('/success', [App\Http\Controllers\OnlineRegistrationController::class, 'success'])->name('success');

    
Auth::routes();
    Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/application-registration', [App\Http\Controllers\ApplicationRegistrationController::class, 'index'])->name('application-registration')->middleware(ClientMiddleware::class);
    Route::get('/application-registration/view/{view_id}', [App\Http\Controllers\ApplicationRegistrationController::class, 'view'])->name('application-registration.view')->middleware(ClientMiddleware::class);
    Route::get('/application-registration/add', [App\Http\Controllers\ApplicationRegistrationController::class, 'add'])->name('application-registration.add')->middleware(ClientMiddleware::class);

    Route::get('/client-manager', [App\Http\Controllers\ClientManagerController::class, 'index'])->name('client-manager')->middleware(ClientMiddleware::class);
    Route::get('/client-manager/view/{view}', [App\Http\Controllers\ClientManagerController::class, 'view'])->name('client-manager.view')->middleware(ClientMiddleware::class);
    Route::get('/client-manager/edit/{edit_id}', [App\Http\Controllers\ClientManagerController::class, 'edit'])->name('client-manager.edit');
    Route::get('/client-manager/process/{process_id}', [App\Http\Controllers\ClientManagerController::class, 'process'])->name('client-manager.process')->middleware(AdminMiddleware::class);;

    Route::get('/application-listing', [App\Http\Controllers\ApplicationListingController::class, 'index'])->name('application-listing')->middleware(AdminMiddleware::class);
    Route::get('/application-listing/view/{view_id}', [App\Http\Controllers\ApplicationListingController::class, 'view'])->name('application-listing.view')->middleware(AdminMiddleware::class);

    Route::get('/user-manager', [App\Http\Controllers\UserManagerController::class, 'index'])->name('user-manager')->middleware(AdminMiddleware::class);
    Route::get('/user-manager/add', [App\Http\Controllers\UserManagerController::class, 'add'])->name('user-manager.add')->middleware(AdminMiddleware::class);
    Route::get('/user-manager/view/{view_id}', [App\Http\Controllers\UserManagerController::class, 'view'])->name('user-manager.view')->middleware(AdminMiddleware::class);


    Route::get('/settings', [App\Http\Controllers\SettingsController::class, 'index'])->name('settings')->middleware(AdminMiddleware::class);
    Route::get('/announcement', [App\Http\Controllers\AnnouncementController::class, 'index'])->name('announcement');
    Route::get('/announcement/add', [App\Http\Controllers\AnnouncementController::class, 'add'])->name('announcement.add');
    Route::get('/announcement/view/{view_id}', [App\Http\Controllers\AnnouncementController::class, 'view'])->name('announcement.view');

    Route::get('/audit-trail', [App\Http\Controllers\AuditTrailController::class, 'index'])->name('audit-trail')->middleware(AdminMiddleware::class);

    
});
